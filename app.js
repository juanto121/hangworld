var express = require("express"),
	app = express(),
	server = require("http").Server(app),
	wordGen = require("./modules/wordgenerator");

app.use(express.static(__dirname+ "/public"));

app.set("port",(process.env.PORT || 80));

var router = express.Router();

router.get("/",function(req,res){
	res.sendStatus(200);
});

router.route("/game/new")
.get(function(req, res){
	res.sendFile(__dirname + "/views/hanged.html");
});


router.route("/game/word")
.get(function(req, res){
	var newWord = wordGen.generate();
	res.json({"word":newWord});
});

app.use("/",router);

app.listen(app.get("port"));