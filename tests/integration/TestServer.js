var serverUrl = "http://localhost";

casper.test.begin("Home page loads", function suite(test){
	casper.start(serverUrl, function(){
		test.assertHttpStatus(200);
	}).run(function(){
		test.done();
	});
});

casper.test.begin("Routes to game page", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertHttpStatus(200);
	}).run(function(){
		test.done();
	});
});

casper.test.begin("New game page has char placeholder", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertExist("div.charPlaceHolder");
	}).run(function(){
		test.done();
	});
});

casper.test.begin("char placeholder has empty underscores at start", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertSelectorHasText("p#word", "_");
	}).run(function(){
		test.done();
	});
});

casper.test.begin("New game page stylesheet exists", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertResourceExists("hangedStyle.css");
	}).run(function(){
		test.done();
	});
});

casper.test.begin("New game page Displays the game State", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertExist("div#gameState");
	}).run(function(){
		test.done();
	});
});

casper.test.begin("New game page Displays Text input", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertExist("input#characterInput");
	}).run(function(){
		test.done();
	});
});

casper.test.begin("Server Word Generator route works", function suite(test){
	casper.start("http://localhost/game/word", function(){
		test.assertHttpStatus(200);
	}).run(function(){
		test.done();
	});
});

casper.test.begin("Server Word Generator generates a word", function suite(test){
	casper.start("http://localhost/game/word", function(){
		var generatedWord = JSON.parse(this.getPageContent());
		test.assertMatch(generatedWord.word, /[a-zA-Z]/i);
	}).run(function(){
		test.done();
	});
});

casper.test.begin("Game page loads hangGame.js", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertResourceExists("hangGame.js");
	}).run(function(){
		test.done();
	});
});

casper.test.begin("Game page loads main.js", function suite(test){
	casper.start("http://localhost/game/new", function(){
		test.assertResourceExists("main.js");
	}).run(function(){
		test.done();
	});
});

/*
casper.test.begin("adds a character and adds to the used div", function suite(test){
	casper.start("http://localhost/game/new");
	casper.then(function(){
		this.sendKeys("input#characterInput");
		this.click("button#checkCharacter");
		var list_chars = $("input#characterInput");
		this.echo(list_chars);
	});
	//test.assertSelectorHasText("ul#usedChars","a");
	casper.run(function(){
		test.done();
	});
});
*/
