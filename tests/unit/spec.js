describe("Hangworld class",function (){
	describe("A new game", function(){
		
		var game;

		beforeEach( function () {
			game = new HangGame();
		}); 

		it("should be defined", function () {
			expect(game).toBeTruthy();
		});

		it("should have the original word", function () {
			expect(game.word).toMatch(/[a-zA-z]/);
		});

		it("should have a game state", function () {
			expect(game.state).toBe(3);
		});

		it("should have a mystery word", function () {
			expect(game.mysteryWord).toBeTruthy();
		});

		it("initializes mystery word correctly", function(){
			game.init();
			expect(game.mysteryWord.length).toBe(game.word.length);
			expect(game.mysteryWord).toMatch(/_+/);
		});

		it("has an associated input" , function () {
			var inputElement = document.createElement("input");
			game = new HangGame({input:inputElement});
			expect(game.input).toBeTruthy();
		});

		it("has an associated button", function () {
			var buttonElement = document.createElement("button");
			game = new HangGame({button:buttonElement});
			expect(game.button).toBeTruthy();
		});

		it("has an associated ul for used chars", function () {
			var ulElement = document.createElement("ul");
			game = new HangGame({usedCharList:ulElement});
			expect(game.usedCharList).toBeTruthy();
		})

		it("has an array of characters used", function(){
			expect(game.usedChars.length).toBe(0);
		});

		it("adds a char to the used array" , function () {
			game.addChar('a');
			expect(game.usedChars[0]).toBe('a');
		});

		it("has the word element", function () {
			var wordElement = document.createElement("p");
			game = new HangGame({wordElement:wordElement});
			expect(game.wordElement).toBeTruthy();
		});

		xit("loads the word from the server", function () {
			game.loadWord();
			expect(game.word).toBe("colombia");
		});

	});
});