var App = (function () {
	function App(){
		this.initVars();
	}

	var app = App.prototype;

	app.initVars = function () {
		this.checkCharButton = $("#checkCharacter");
		this.inputChar = $("#characterInput");
		this.usedCharList = document.querySelector("#usedChars");
		this.wordElement = document.querySelector("#word");
		this.hangGame = new HangGame({ input:this.inputChar,
									   button:this.checkCharButton,
									   usedCharList:this.usedCharList,
									   wordElement:this.wordElement
									});
	}

	

	return App;

})();

window.onload = function(){
	main = new App();
};