var HangGame = ( function () {
	function HangGame(config){
		this.initVars(config);
		this.initEvents();
	}

	var game = HangGame.prototype;

	game.initVars = function (config) {
		this.word = "a";
		this.mysteryWord = "_";
		this.state = 3;
		this.usedChars = [];

		if(config){
			this.input  = config.input  || null;
			this.button = config.button || null;
			this.usedCharList = config.usedCharList || null;
			this.wordElement = config.wordElement || null;
		}
		this.init();
	};

	game.useChar = function(){
		var character = this.input[0].value;
		this.addChar(character);
		var newUsedChar = document.createElement("li");
		newUsedChar.className = "charItem";
		newUsedChar.textContent = character;
		this.usedCharList.appendChild(newUsedChar);
		this.revealChars(character);
		this.updateGameState(character);
	};

	game.addChar = function (character) {
		this.usedChars.push(character);
	}

	game.init = function (config) {
		this.loadWord(this.showWord.bind(this));
	};

	game.loadWord = function(callback){
		$.get("http://localhost/game/word", function(data, status){
			callback(data.word);
		});
	};

	game.showWord = function (word) {
		this.mysteryWord = word.replace(/\w/g,"_ ");
		this.word = word;
		this.wordElement.textContent = this.mysteryWord;
	};

	game.updateGameState = function (character){

	};

	game.revealChars = function (character) {
		var len = this.word.length;
		var tempMysteryWord = "";
		for(var i = 0; i < len; i++){
			var wordMatch = this.word.charAt(i);
			if(wordMatch == character){
				tempMysteryWord += wordMatch + " ";

			}else{
				tempMysteryWord += "_ ";
			}
		}

		for(var i = 0; i < this.mysteryWord.length; i++){
			var mw = this.mysteryWord.charAt(i);
			var tw = tempMysteryWord.charAt(i);
			if(mw!=tw){
				if(mw == "_")
					this.mysteryWord = this.mysteryWord.substring(0,i) + tw + this.mysteryWord.substring(i+1);
				else
					this.mysteryWord[i] = this.mysteryWord.substring(0,i) + mw + this.mysteryWord.substring(i+1);
			}
		}

		this.wordElement.textContent = this.mysteryWord;

	}

	game.initEvents = function () {
		this.button.on("click", this.useChar.bind(this));
	};

	return HangGame;
})();